from django.urls import path, re_path, include
from .views import index,add_friend

urlpatterns = [
    path('', index, name='index'),
    path('add', add_friend, name='add'),
]
