# Pertanyaan Singkat Lab_2

- Nama  : Dzulfikar Rachman
- NPM   : 2006523086
- Kelas : PBP - F

---

1. Apakah perbedaan antara JSON dan XML?

JSON (JavaScript Object Notation) dan XML (eXtensible Markup Language)
memiliki fungsi yang sama yakni untuk emnyimpan dan mengirim data.
Perbedaan mendasar antara keduanya adalah bagaimana cara data disimpan.
JSON menyimpan data dalam bentuk sepasang _key_ dan _value_ sedangkan
pada XML data dibungkus oleh tag dan membentuk struktur data tree. JSON
hanya mendukung tipe data primitif sedangkan XML mendukung banyak tipe
data, baik primitif maupun kompleks. Pada XML kita dapat memproses atau memformat
data, berbeda dengan JSON yang tidak dapat melakukan proses apapun.

2. Apakah perbedaan antara HTML dan XML?

HTML (HyperText Markup Language) adalah sebuah bahasa _markup_ yang digunakan sebagai
struktur dari halaman _website_. Sedangkan XML merupakan sebuah bahasa _markup_ yang
berfokus pada pertukaran dan penyimpanan data.