from django import forms
from lab_2.models import Note


class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ('penerima', 'pengirim', 'judul', 'pesan')

        widgets = {
            'penerima': forms.TextInput(
                attrs={'class': 'form-control', 'placeholder': 'Nama Tujuan'}),

            'pengirim': forms.TextInput(
                attrs={'class': 'form-control', 'placeholder': 'Nama Anda'}),

            'judul': forms.TextInput(
                attrs={'class': 'form-control', 'placeholder': 'Subjek Pesan'}),

            'pesan': forms.Textarea(
                attrs={'class': 'form-control', 'placeholder': 'Pesan Anda'}),
        }
        