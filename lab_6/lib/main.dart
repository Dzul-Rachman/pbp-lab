import 'package:flutter/material.dart';

void main() {
  runApp(const KladafDiary());
}

class KladafDiary extends StatelessWidget {
  const KladafDiary({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Kladaf',
      theme: _buildTheme(),
      home: const MyHomePage(title: 'Kladaf'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  int _selectedIndex = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  _dummyDiaryModal() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
            child: Container(
              constraints: const BoxConstraints(maxHeight: 350),
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    RichText(
                      textAlign: TextAlign.justify,
                      text: const TextSpan(
                          text:
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ut massa id velit sagittis iaculis. Aliquam accumsan tortor id turpis porttitor mollis. Suspendisse ac nibh et lorem interdum suscipit ac in quam. Vestibulum nec nunc quam. Donec eu diam et nisl rhoncus convallis. Donec ultricies mauris urna, ut pellentesque nunc vulputate at. Duis pellentesque vehicula tellus in rhoncus. Sed hendrerit erat ac quam bibendum pulvinar. Sed ut nulla mattis, cursus nunc dictum, malesuada lacus. Etiam porta nibh enim, at ultrices justo convallis id. Vestibulum fermentum mollis risus, id congue lacus posuere porttitor. Integer convallis venenatis varius. Maecenas sit amet nisl et urna lobortis efficitur. Integer viverra rhoncus nulla quis dictum.',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              color: Colors.black,
                              wordSpacing: 1)),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
        backgroundColor: const Color.fromRGBO(29, 45, 68, 1),
        actions: const [
          Icon(Icons.account_circle),
          Padding(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Icon(Icons.more_vert_rounded)),
        ],
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 300,
              height: 200,
              padding: const EdgeInsets.all(10.0),
              child: Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0)),
                elevation: 10,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const ListTile(
                      leading: Icon(Icons.library_books_rounded),
                      title: Text('What A Great Day'),
                      subtitle: Text("17-08-2021"),
                    ),
                    ButtonBar(
                      children: <Widget>[
                        ElevatedButton(
                          child: const Text('View'),
                          onPressed: _dummyDiaryModal,
                        ),
                        ElevatedButton(
                            child: const Text('Delete'),
                            onPressed: _incrementCounter)
                      ],
                    )
                  ],
                ),
              ),
            ),
            Container(
              width: 300,
              height: 200,
              padding: const EdgeInsets.all(10.0),
              child: Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0)),
                elevation: 10,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const ListTile(
                      leading: Icon(Icons.library_books_rounded),
                      title: Text('What A Boring Day'),
                      subtitle: Text("31-08-2021"),
                    ),
                    ButtonBar(
                      children: <Widget>[
                        ElevatedButton(
                          child: const Text('View'),
                          onPressed: _dummyDiaryModal,
                        ),
                        ElevatedButton(
                            child: const Text('Delete'),
                            onPressed: _incrementCounter)
                      ],
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.

      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.my_library_books_rounded),
            label: 'Diary',
            backgroundColor: Color(0xff1d2d44),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.access_time_rounded),
            label: 'Podomoro Timer',
            backgroundColor: Color(0xff1d2d44),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.calendar_today_rounded),
            label: 'Schedule & Tracker',
            backgroundColor: Color(0xff1d2d44),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.star_border_rounded),
            label: 'Wishlist',
            backgroundColor: Color(0xff1d2d44),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: const Color.fromRGBO(245, 245, 245, 1),
        onTap: _onItemTapped,
      ),
    );
  }
}

ThemeData _buildTheme() {
  final ThemeData base = ThemeData.dark();
  return base.copyWith(
    colorScheme: _buildColorScheme,
    textTheme: _buildTextTheme(base.textTheme),
  );
}

TextTheme _buildTextTheme(TextTheme base) {
  return base
      .copyWith(
        caption: base.caption!.copyWith(
          fontWeight: FontWeight.w400,
          fontSize: 14,
          letterSpacing: 0.03,
        ),
        button: base.button!.copyWith(
          fontWeight: FontWeight.w500,
          fontSize: 14,
          letterSpacing: 0.03,
        ),
      )
      .apply(
        fontFamily: 'Roboto',
      );
}

const ColorScheme _buildColorScheme = ColorScheme(
  primary: darkBlack,
  primaryVariant: darkBlack,
  secondary: medGray,
  secondaryVariant: lightGray,
  surface: oldPaperWhite,
  background: darkBlack,
  error: medGray,
  onPrimary: lightGray,
  onSecondary: oldPaperWhite,
  onSurface: oldPaperWhite,
  onBackground: lightGray,
  onError: oldPaperWhite,
  brightness: Brightness.dark,
);

const Color oldPaperWhite = Color(0xfff0ebd8);
const Color lightGray = Color(0xff748cab);
const Color medGray = Color(0xff3e5c76);
const Color darkGray = Color(0xff1d2d44);
const Color darkBlack = Color(0xff0d1321);
